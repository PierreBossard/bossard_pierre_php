<?php


namespace App\DataFixtures;


use App\Entity\Cloth;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class clothFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $cloth = new Cloth();
        $cloth->setTitle("Classic")
            ->setDescription('Tee shirt Classic')
            ->setPrice(10)
            ->setCategorieID(2)
            ->setPhoto('https://zupimages.net/up/20/18/wisy.jpg')
            ->setQuantity(35);
    }
}
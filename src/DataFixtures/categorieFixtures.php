<?php


namespace App\DataFixtures;

use App\Entity\Categorie;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class categorieFixtures extends Fixture
{
    public function load(ObjectManager $manager){
        $longSleeve = new Categorie();
        $longSleeve->setTheme("Long-Sleeve") ;

        $manager->persist($longSleeve);
        $manager->flush();

        $shortSleeve = new Categorie();
        $shortSleeve->setTheme("Short-Sleeve");

        $manager->persist($shortSleeve);
        $manager->flush();

        $hoodie = new Categorie();
        $hoodie->setTheme("Hoodie");

        $manager->persist($hoodie);
        $manager->flush();

        $sweat = new Categorie();
        $sweat->setTheme("Sweat");

        $manager->persist($sweat);
        $manager->flush();
    }
}

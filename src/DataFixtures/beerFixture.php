<?php


namespace App\DataFixtures;


use App\Entity\Beer;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class beerFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $cloth = new Beer();
        $cloth->setTitle("Chouffe")
            ->setDescription('Good beer')
            ->setPrice(3)
            ->setPhoto('https://images.interdrinks.com/v5/img/p/1481-22222-w786-h540-transparent.png')
            ->setQuantity(60);
    }
}
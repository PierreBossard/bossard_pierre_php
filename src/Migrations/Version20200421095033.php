<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200421095033 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE categorie (id INT AUTO_INCREMENT NOT NULL, categorie_id INT NOT NULL, categorie_theme VARCHAR(100) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE client (id INT AUTO_INCREMENT NOT NULL, client_id INT NOT NULL, name VARCHAR(100) NOT NULL, client_adresse VARCHAR(255) DEFAULT NULL, client_ville VARCHAR(100) DEFAULT NULL, client_cp INT DEFAULT NULL, client_tel INT DEFAULT NULL, client__email VARCHAR(200) DEFAULT NULL, client_pass VARCHAR(255) NOT NULL, client_statut VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE contact (id INT AUTO_INCREMENT NOT NULL, contact_id INT NOT NULL, contact_date DATETIME NOT NULL, contat_client_id INT NOT NULL, contact_message VARCHAR(255) DEFAULT NULL, contact_statut VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `order` (id INT AUTO_INCREMENT NOT NULL, order_id INT NOT NULL, order_date DATETIME NOT NULL, order_client_id INT NOT NULL, order_etat VARCHAR(100) NOT NULL, order_total INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product (id INT AUTO_INCREMENT NOT NULL, id_product INT NOT NULL, title VARCHAR(100) NOT NULL, product_description VARCHAR(255) DEFAULT NULL, product_categorie_id INT NOT NULL, price DOUBLE PRECISION NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('DROP TABLE clients');
        $this->addSql('ALTER TABLE brands CHANGE brand_name brand_name VARCHAR(100) DEFAULT NULL');
        $this->addSql('ALTER TABLE cars CHANGE car_brand car_brand INT DEFAULT NULL, CHANGE car_visible car_visible TINYINT(1) DEFAULT NULL, CHANGE car_model car_model VARCHAR(100) DEFAULT NULL, CHANGE car_price car_price NUMERIC(10, 2) DEFAULT NULL');
        $this->addSql('ALTER TABLE choices CHANGE cho_question cho_question INT DEFAULT NULL, CHANGE cho_visible cho_visible TINYINT(1) DEFAULT NULL, CHANGE cho_text cho_text VARCHAR(100) DEFAULT NULL, CHANGE cho_numvotes cho_numvotes INT DEFAULT NULL');
        $this->addSql('ALTER TABLE questions CHANGE qu_text qu_text VARCHAR(100) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE clients (client_id INT AUTO_INCREMENT NOT NULL, client_name VARCHAR(100) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, client_adresse VARCHAR(100) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, client_ville VARCHAR(100) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, client_cp INT NOT NULL, client_tel INT NOT NULL, client_email VARCHAR(100) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, client_pass VARCHAR(200) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, client_statut TINYINT(1) DEFAULT \'NULL\', UNIQUE INDEX UNIQ_C82E748FBFBD64 (client_name), PRIMARY KEY(client_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('DROP TABLE categorie');
        $this->addSql('DROP TABLE client');
        $this->addSql('DROP TABLE contact');
        $this->addSql('DROP TABLE `order`');
        $this->addSql('DROP TABLE product');
        $this->addSql('ALTER TABLE brands CHANGE brand_name brand_name VARCHAR(100) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE cars CHANGE car_brand car_brand INT DEFAULT NULL, CHANGE car_visible car_visible TINYINT(1) DEFAULT \'NULL\', CHANGE car_model car_model VARCHAR(100) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE car_price car_price NUMERIC(10, 2) DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE choices CHANGE cho_question cho_question INT DEFAULT NULL, CHANGE cho_visible cho_visible TINYINT(1) DEFAULT \'NULL\', CHANGE cho_text cho_text VARCHAR(100) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE cho_numvotes cho_numvotes INT DEFAULT NULL');
        $this->addSql('ALTER TABLE questions CHANGE qu_text qu_text VARCHAR(100) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
    }
}

<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200423142554 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE beer ADD title VARCHAR(255) NOT NULL, ADD description VARCHAR(255) NOT NULL, ADD categorie_id INT DEFAULT NULL, ADD price INT NOT NULL, ADD photo VARCHAR(255) NOT NULL, DROP beer_id, DROP beer_title, DROP beer_description, DROP beer_categorie_id, DROP beer_price, DROP beer_photo');
        $this->addSql('ALTER TABLE brands CHANGE brand_name brand_name VARCHAR(100) DEFAULT NULL');
        $this->addSql('ALTER TABLE cars CHANGE car_brand car_brand INT DEFAULT NULL, CHANGE car_visible car_visible TINYINT(1) DEFAULT NULL, CHANGE car_model car_model VARCHAR(100) DEFAULT NULL, CHANGE car_price car_price NUMERIC(10, 2) DEFAULT NULL');
        $this->addSql('ALTER TABLE choices CHANGE cho_question cho_question INT DEFAULT NULL, CHANGE cho_visible cho_visible TINYINT(1) DEFAULT NULL, CHANGE cho_text cho_text VARCHAR(100) DEFAULT NULL, CHANGE cho_numvotes cho_numvotes INT DEFAULT NULL');
        $this->addSql('ALTER TABLE client CHANGE client_adresse client_adresse VARCHAR(255) DEFAULT NULL, CHANGE client_ville client_ville VARCHAR(100) DEFAULT NULL, CHANGE client_cp client_cp INT DEFAULT NULL, CHANGE client_tel client_tel INT DEFAULT NULL, CHANGE client__email client__email VARCHAR(200) DEFAULT NULL');
        $this->addSql('ALTER TABLE cloth CHANGE categorie_id categorie_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE contact CHANGE date date DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE product CHANGE product_description product_description VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE questions CHANGE qu_text qu_text VARCHAR(100) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE beer ADD beer_title VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, ADD beer_description VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, ADD beer_categorie_id INT DEFAULT NULL, ADD beer_price INT NOT NULL, ADD beer_photo VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, DROP title, DROP description, DROP categorie_id, DROP photo, CHANGE price beer_id INT NOT NULL');
        $this->addSql('ALTER TABLE brands CHANGE brand_name brand_name VARCHAR(100) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE cars CHANGE car_brand car_brand INT DEFAULT NULL, CHANGE car_visible car_visible TINYINT(1) DEFAULT \'NULL\', CHANGE car_model car_model VARCHAR(100) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE car_price car_price NUMERIC(10, 2) DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE choices CHANGE cho_question cho_question INT DEFAULT NULL, CHANGE cho_visible cho_visible TINYINT(1) DEFAULT \'NULL\', CHANGE cho_text cho_text VARCHAR(100) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE cho_numvotes cho_numvotes INT DEFAULT NULL');
        $this->addSql('ALTER TABLE client CHANGE client_adresse client_adresse VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE client_ville client_ville VARCHAR(100) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE client_cp client_cp INT DEFAULT NULL, CHANGE client_tel client_tel INT DEFAULT NULL, CHANGE client__email client__email VARCHAR(200) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE cloth CHANGE categorie_id categorie_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE contact CHANGE date date DATETIME DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE product CHANGE product_description product_description VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE questions CHANGE qu_text qu_text VARCHAR(100) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
    }
}

<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200501082009 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE beer CHANGE categorie_id categorie_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE brands CHANGE brand_name brand_name VARCHAR(100) DEFAULT NULL');
        $this->addSql('ALTER TABLE cars CHANGE car_brand car_brand INT DEFAULT NULL, CHANGE car_visible car_visible TINYINT(1) DEFAULT NULL, CHANGE car_model car_model VARCHAR(100) DEFAULT NULL, CHANGE car_price car_price NUMERIC(10, 2) DEFAULT NULL');
        $this->addSql('ALTER TABLE choices CHANGE cho_question cho_question INT DEFAULT NULL, CHANGE cho_visible cho_visible TINYINT(1) DEFAULT NULL, CHANGE cho_text cho_text VARCHAR(100) DEFAULT NULL, CHANGE cho_numvotes cho_numvotes INT DEFAULT NULL');
        $this->addSql('ALTER TABLE client CHANGE client_adresse client_adresse VARCHAR(255) DEFAULT NULL, CHANGE client_ville client_ville VARCHAR(100) DEFAULT NULL, CHANGE client_cp client_cp INT DEFAULT NULL, CHANGE client_tel client_tel INT DEFAULT NULL, CHANGE client__email client__email VARCHAR(200) DEFAULT NULL');
        $this->addSql('ALTER TABLE cloth CHANGE categorie_id categorie_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE contact CHANGE date date DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE product CHANGE product_description product_description VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE questions CHANGE qu_text qu_text VARCHAR(100) DEFAULT NULL');
        $this->addSql('ALTER TABLE user ADD roles JSON NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE beer CHANGE categorie_id categorie_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE brands CHANGE brand_name brand_name VARCHAR(100) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE cars CHANGE car_brand car_brand INT DEFAULT NULL, CHANGE car_visible car_visible TINYINT(1) DEFAULT \'NULL\', CHANGE car_model car_model VARCHAR(100) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE car_price car_price NUMERIC(10, 2) DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE choices CHANGE cho_question cho_question INT DEFAULT NULL, CHANGE cho_visible cho_visible TINYINT(1) DEFAULT \'NULL\', CHANGE cho_text cho_text VARCHAR(100) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE cho_numvotes cho_numvotes INT DEFAULT NULL');
        $this->addSql('ALTER TABLE client CHANGE client_adresse client_adresse VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE client_ville client_ville VARCHAR(100) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE client_cp client_cp INT DEFAULT NULL, CHANGE client_tel client_tel INT DEFAULT NULL, CHANGE client__email client__email VARCHAR(200) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE cloth CHANGE categorie_id categorie_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE contact CHANGE date date DATETIME DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE product CHANGE product_description product_description VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE questions CHANGE qu_text qu_text VARCHAR(100) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE user DROP roles');
    }
}

<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ProductRepository;


class ProductController extends AbstractController
{

    /**
     * @Route("/products", name="product_index")
     */
    public function index(ProductRepository $productRepository)
    {
        return $this->render('product/product.html.twig', [
            'products' => $productRepository->findAll()
        ]);
    }
}


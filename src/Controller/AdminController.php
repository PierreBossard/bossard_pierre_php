<?php


namespace App\Controller;

use App\Entity\Cloth;
use App\Entity\Beer;
use App\Entity\User;
use App\Form\AddProductType;
use App\Repository\BeerRepository;
use App\Repository\ClothRepository;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/admin", name="admin_")
 */

class AdminController extends AbstractController
{
    /**
     * @Route("/newCloth", name="new_cloth")
     * @param Request $request
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function AddCloth(Request $request, EntityManagerInterface $manager, ClothRepository $cloths)
    {
        $cloth = new Cloth();

        $formCloth = $this->createForm(AddProductType::class, $cloth);
        $formCloth->handleRequest($request);

        if ($formCloth->isSubmitted() && $formCloth->isValid()) {

            $manager->persist($cloth);
            $manager->flush();

            return $this->render('admin/addCloth.html.twig', [
                'formCloth' => $formCloth->createView(), 'cloths' => $cloths->findAll()]);
        }

        return $this->render('admin/addCloth.html.twig', ['formCloth' => $formCloth->createView(), 'cloths' => $cloths->findAll()]);
    }
        /**
         * @Route("/newBeer", name="new_beer")
         * @param Request $request
         * @param ObjectManager $manager
         * @return \Symfony\Component\HttpFoundation\Response
         */
        public function AddBeer(Request $request, EntityManagerInterface $manager, BeerRepository $beers)
        {
            $beer = new Beer();

            $formBeer = $this->createForm(AddProductType::class, $beer);

            $formBeer->handleRequest($request);

            if ($formBeer->isSubmitted() && $formBeer->isValid()) {

                $manager->persist($beer);
                $manager->flush();

                return $this->render('admin/addBeer.html.twig', [
                    'formBeer' => $formBeer->createView(),'beers' => $beers->findAll()]);
            }

            return $this->render('admin/addBeer.html.twig', ['formBeer' => $formBeer->createView(),'beers' => $beers->findAll()]);
        }

    /**
     * @Route("/users", name="users")
     * @param UserRepository $users
     * @return \Symfony\Component\HttpFoundation\Response
     */
        public function usersList(UserRepository $users)
        {
            return $this->render('admin/users.html.twig', [
                'users' => $users->findAll(),
            ]);
        }

    /**
     * @Route("/deleteCloth/{id}", name="cloth_delete")
     * @return \Symfony\Component\HttpFoundation\Response
     */
        public function deleteCloth(Cloth $cloth){

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($cloth);
            $entityManager->flush();

            return $this->redirectToRoute("admin_new_cloth");
        }

    /**
 * @Route("/deleteBeer/{id}", name="beer_delete")
 * @return \Symfony\Component\HttpFoundation\Response
 */
    public function deleteBeer(Beer $beer){

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($beer);
        $entityManager->flush();

        return $this->redirectToRoute("admin_new_beer");
    }

    /**
     * @Route("/deleteUser/{id}", name="user_delete")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteUser(User $user){

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($user);
        $entityManager->flush();

        return $this->redirectToRoute("admin_users");
    }
}
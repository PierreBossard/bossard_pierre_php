<?php

namespace App\Controller;

use App\Repository\ClothRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ClothController extends AbstractController
{
    /**
     * @Route("/cloth", name="cloth_index")
     */
    public function index(ClothRepository $clothRepository)
    {
        return $this->render('product/cloth.html.twig', [
            'cloths' => $clothRepository->findAll()
        ]);
    }
}

<?php

namespace App\Controller;

use App\Repository\BeerRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class BeerController extends AbstractController
{
    /**
     * @Route("/beer", name="beer_index")
     */
    public function index(BeerRepository $beerRepository)
    {
        return $this->render('product/beer.html.twig', [
            'beers' => $beerRepository->findAll()
        ]);
    }
}

<?php

namespace App\Controller;



use App\Entity\Cart;
use App\Entity\User;
use App\Repository\ClothRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;


class CartController extends AbstractController
{
    /**
     * @Route("/cart", name="cart_index")
     */
    public function index(SessionInterface $session, ClothRepository $clothRepository)
    {
        $panier = $session->get('panier', []);

        $panierWithData = [];

        foreach ($panier as $id => $quantity) {
            $panierWithData[] = [
                'cloth' => $clothRepository->find($id),
                'quantity' => $quantity
            ];
        }

        $total = 0;

        foreach ($panierWithData as $item)
        {
            $totalItem = $item['cloth']->getPrice()*$item['quantity'];
            $total += $totalItem;
        }

        return $this->render('order/order.html.twig', [
            'items' => $panierWithData,
            'total' => $total
        ]);

    }

    /**
     * @Route("/cart/add/{id}", name="cart_add")
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function add($id, SessionInterface $session) {

        $panier = $session->get('panier', []);

        if(!empty($panier[$id])) {
            $panier[$id]++;
        } else {
            $panier[$id] = 1;
        }

        $session->set('panier',$panier);

        return $this->redirectToRoute("cloth_index");
    }

    /**
     * @Route("/cart/remove/{id}", name="cart_remove")
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function remove($id, SessionInterface $session) {

        $panier = $session->get('panier', []);

        if(!empty($panier[$id])) {
            unset($panier[$id]);
        }

        $session->set('panier',$panier);
        return $this->redirectToRoute("cart_index");
    }

    /**
     * @Route("/saveCart", name="cart_save")
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @param SessionInterface $session
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function save(Request $request, EntityManagerInterface $manager, SessionInterface $session) {
        $saveCart = new Cart();

        $panier = $session->get('panier', []);
        $username = $session -> getName();
        $total = $session->get('panier',['total']);

        $saveCart->setUser($username);
        $saveCart->setItem($panier);
        $saveCart->setTotal($total);

        $manager->persist($saveCart);
        $manager->flush();

        return $this ->redirectToRoute('cloth_index');
    }
}
